//Show databases:
show dbs

//Create a database:

use myFirstDB
db.users.insertOne({name: "Juan Dela Cruz", age: 20, isActive: true})

//Check results:
db.users.find()

//add .pretty() to format results

//add a new document:
db.users.insertOne({name: "Jino Yumul", age: 31, isActive: false})

//Within each database is a collection, where multiple related data is stored

//Create a collection
db.products.insertMany([
		{ 
			name: "Product 1", price: 200.50, stock: 100, description: "lorem ipsum"
		},
		{
			name: "Product 2", price: 333, stock: 25, description: "lorem ipsum"
		}
	])

//Find a specific document
db.products.find({"name": "Product 2"})

//Update a document

db.users.find({"name": "Jino Yumul"})

db.users.updateOne(
	{
		"_id": ObjectId("5f48e0a1797409282ba5014b")
	},
	{
		$set: {"isActive": true}
	}
)

//Update and add new information

db.users.updateOne(
  {"_id" : ObjectId("5f48e0a1797409282ba5014b") },
  {
    $set: {
      "address": {
        barangay: "Sacred Heart",
        city: "Quezon City",
        province: "Metro Manila",
        country: "Philippines"
      }
    }
  }
)

//Delete a document

db.users.deleteOne({"_id": ObjectId("5f48dc6dc915acef95b34be5")})

//CRUD
//Create, retrieve, update, delete

User ID:
ObjectId("5f48e0a1797409282ba5014b")

Product ID:
ObjectId("5f48f9786a0a9d3741c028fe")

db.orders.insertOne(
	{
		user_id: ObjectId("5f48e0a1797409282ba5014b"),
		product_id: ObjectId("5f48f9786a0a9d3741c028fe"),
		qty: 3
	}
)