//MongoDB CRUD:

//Find all listings that can accomodate 4 or more people, have at least 2 beds, 2 bathrooms, and have a flexible cancellation policy.

db.listingsAndReviews.find(
	{
		accommodates: {$gte: 4}, //$gte = greater than or equal to operator
		beds: {$gte: 2},
		bathrooms: {$gte: 2},
		cancellation_policy: "flexible"
	},
	{
		listing_url: 1,
		name: 1,
		accommodates: 1,
		beds: 1,
		bathrooms: 1,
		cancellation_policy: 1
	}
)
.limit(5)
.sort({name: 1}) //sort results

//Listings that are of property type condominium, has a minimum stay of 1 night, and can accommodate 2 people

db.listingsAndReviews.find(
	{
		property_type: "Condominium",
		minimum_nights: "1",
		bedrooms: 1,
		accommodates: 2
	},
	{
		name: 1,
		property_type: 1,
		minimum_nights: 1,
		bedrooms: 1,
		accommodates: 1
	}
)
.limit(5)

//Listings that have a price less than 100, fee for additional heads that is less than 20, and are in Portugal

db.listingsAndReviews.find(
	{
		price: {$lt: 100}, //$lt operator = less than
		extra_people: {$lt: 20},
		"address.country": "Portugal"
	},
	{
		name: 1,
		price: 1,
		extra_people: 1,
		"address.country": 1
	}
)
.limit(5)

//listings that have ALL of the following amenities:
//Cable TV, Wifi, Washer, Essentials, Bed Linens

db.listingsAndReviews.find(
	{
		amenities: {
			$all: ["Cable TV", "Wifi", "Washer", "Essentials", "Bed linens"]
			//$all to find ALL values in the query array
			//$in to find ANY of the values in the query array
		} 
	},
	{
		name: 1,
		amenities: 1
	}
)
.limit(5)

db.listingsAndReviews.find(
	{},
	{
		name: 1,
		amenities: 1
	}
)
.limit(5)

//UPDATE

//Update all listings to have an additional amenity "Fiber internet"

db.listingsAndReviews.updateMany(
	{},
	{
		// $push: {amenities: "Fiber internet"}
		$push: {
			amenities: {
				$each: ["Fiber internet", "Breakfast"]
			}
		}
	}
)

// db.listingsAndReviews.find(
// 	{},
// 	{
// 		name: 1,
// 		amenities: 1
// 	}
// )
// .limit(5)

//Increase the price of all listings whose prices are less than 100 by 10

db.listingsAndReviews.updateMany(
	{
		price: {$lt: 100}
	},
	{
		$inc: {price: 10} //$inc = increment
	}
)

//DELETE RECORDS

//Delete listings with any of the following conditions:
//review scores accuracy <=75
//review scores cleanliness <=80
//review scores checkin <=70

db.listingsAndReviews.deleteMany(
	{
		$or: //$or = "Or" operator
		[
			{
				"review_scores.review_scores_accuracy": {$lte: 75}
			},

			{
				"review_scores.review_scores_cleanliness": {$lte: 80}
			},

			{
				"review_scores.review_scores_checkin": {$lte: 70}
			}

		]
	}
)